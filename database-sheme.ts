interface IUser {
    _id: number;
    email: string;
    password: string;
    phone: string;
    siteId: number;
}

interface ISite {
    _id: number;
    origins: [string],
    created: number
}

interface ILink {
    link: string,
    siteId: number;
}

interface ICompany {
    _id: number;
    siteId: number;
    logo: string;
    promo: string;
    addresses: [
        {
            title: string,
            address: string,
            time: {
                from: number,
                to: number
            },
            coords: [number, number]
        }
        ];
    delivery: string;
}

interface ICategory {
    _id: number;
    siteId: number;
    title: string;
}

interface IGallery {
    _id: number;
    siteId: number;
    img: string;
}

interface INews {
    _id: number;
    siteId: number;
    text: string;
    img: string;
}

interface IProduct {
    _id: number;
    siteId: number;
    title: string;
    img: string;
    description: string;
    price: number;
    categories: Array<string>;;
    images: Array<string>;
}