import * as React from 'react';
import { Link } from 'react-router-dom';
import BaseComponent from '../../components/base-component';
import './header.scss';
import * as classnames from 'classnames';
import Basket from '../../components/basket/basket';
import { modalService } from '../../services/modal-service';
import { companyService, ICompanyInfo } from '../../services/company-service';
import Button from "../../components/button/button";
import {orgService} from '../../services/org-service';

declare const globals: any;

let headerMenu = [
    {
        title: 'Магазин',
        url: '/'
    },
    {
        title: 'Галерея',
        url: '/gallery'
    },
    {
        title: 'Новости',
        url: '/news'
    },
    {
        title: 'Оплата и доставка',
        url: '/delivery',
        name: 'delivery'
    },
    {
        title: 'Контакты',
        url: '/contacts'
    }
];

class Header extends BaseComponent {
    public state: any = {
        selectedIndex: -1,
        menuExpanded: false,
        showBuyButton: false,
        companyInfo: {}
    };

    public componentDidMount() {
        companyService.getInfo()
            .then((companyInfo: ICompanyInfo) => {
                if(companyInfo.hideDelivery) {
                    headerMenu = headerMenu.filter((item: any) => {
                        return item.name !== 'delivery';
                    });
                    this.forceUpdate();
                }
                this.setState({companyInfo});
            });
        orgService.getPublicOrg()
            .then((org: any) => {
                if(!org.isPayed) {
                    this.setState({showBuyButton: true})
                }
            });

        if(globals.type === 'temp') {
            this.setState({showBuyButton: true});
        }

        this.on('event:route:change', (location: any) => {
            const selectedIndex = headerMenu.findIndex((item: any) => {
                return item.url === location.pathname;
            });
            this.setState({selectedIndex});
        });
    }

    public menuToggle() {
        const menuExpanded = !this.state.menuExpanded;
        if(menuExpanded) {
            const docListener = () => {
                document.removeEventListener('click', docListener);
                this.setState({menuExpanded: false});
            };
            document.addEventListener('click', docListener);
            this.setState({menuExpanded});
        }
    }

    private redirectToMain = () => {
        // @ts-ignore
        const groupName = location.host.match(/^[^\.]+/)[0].replace(/_tmp$/, '');
        location.href = `https://litestore.me/main?authFailUrl=${encodeURIComponent(`/authorize?register&groupName=${groupName}`)}`;
    };

    public render() {
        const openBasket = () => {
            modalService.open((<Basket/>));
        };
        const logoStyle = {
            backgroundImage: `url(${this.state.companyInfo.logo})`
        };
        return (
            <div>
                <header className="header">
                    <div className="header__container">
                        {!this.state.companyInfo.hideLogo &&
                        <div className="header__link-container_unselected">
                            <div className="header__logo" style={logoStyle}/>
                        </div>}
                        {this.renderLinks()}
                        <div className="header__link-container" onClick={openBasket}>
                            <img className="header__link-basket" src="/static/images/basket.png"/>
                            <span className="header__link-text">Корзина</span>
                        </div>
                        {this.state.showBuyButton &&
                        <div className="header__link-container header__redirect">
                            <Button text="Панель управления"
                                    onClick={this.redirectToMain}/>
                        </div>}
                        <div className="header__menu" onClick={this.menuToggle.bind(this)}>
                            <svg id="icon-burger" viewBox="0 0 24 24" width="100%" height="100%"><rect y="2" width="24" height="2" rx="1"></rect><rect y="20" width="24" height="2" rx="1"></rect><rect y="8" width="24" height="2" rx="1"></rect><rect y="14" width="24" height="2" rx="1"></rect></svg>
                            <div className={classnames('header__menu-dropdown', {'header__menu-dropdown_expanded': this.state.menuExpanded})}>
                                {this.renderLinks()}
                                <div className="header__link-container" onClick={openBasket}>
                                    <span className="header__link-text">Корзина</span>
                                    <img className="header__link-basket" src="/static/images/basket.png"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                {this.state.showBuyButton &&
                <div className="header__bottom">
                    <div className="header__container header__container_bottom">
                        <Button text="Панель управления"
                                onClick={this.redirectToMain}/>
                    </div>
                </div>}
            </div>
        );
    }

    private renderLinks() {
        return headerMenu.map((item: any, key: number) => {
            return (
                <Link key={key}
                      className={classnames('header__link-container', {'header__link-container_selected': this.state.selectedIndex === key})}
                      to={item.url}>
                    <span className="header__link-text">{item.title}</span>
                </Link>
            );
        });
    }
}

export default Header;
