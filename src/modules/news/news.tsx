import * as React from 'react';
import BaseComponent from '../../components/base-component';
import { newsService, INewsItem } from '../../services/news-service';
import { modalService } from '../../services/modal-service';
import PictureViewer from '../../components/picture-viewer/picture-viewer';
import './news.scss';
import Loader from "../../components/loader/loader";
import ScrollFetch from "../../components/scroll-fetch/scroll-fetch";
import * as classnames from 'classnames';
import * as moment from 'moment';
import 'moment/locale/ru';
import { productsService, IProduct } from '../../services/products-service';

interface IState {
    news: Array<INewsItem>;
    products: Array<IProduct>;
    isFetching: boolean;
    isFetchingAllowed: boolean;
    expandedNews: Set<number>;
}

class News extends BaseComponent {

    public state: IState = {
        news: [],
        products: [],
        isFetching: false,
        isFetchingAllowed: true,
        expandedNews: new Set()
    };

    private offset: number = 0;
    private limit: number = 18;

    public componentDidMount() {
        this.getNews();
        productsService.list({limit: 5})
            .then((products: Array<IProduct>) => {
                this.setState({products});
            });
    }

    public render() {
        const onFetch = () => this.getNews();
        return (
            <div className="news">
                <div>
                    <div className="news__wrapper">
                        {this.renderNews()}
                    </div>
                    {this.state.isFetching &&
                    <Loader/>}
                    {this.state.isFetchingAllowed &&
                    <ScrollFetch onFetch={onFetch}/>}
                </div>
                <div className="news__products">
                    <div className="news__products-title">Недавно добавлены</div>
                    {this.renderProducts()}
                </div>
            </div>
        );
    }

    private renderProducts() {
        return this.state.products.map((item: IProduct) => {
            const style = {
                backgroundImage: `url(${item.image})`
            };
            const click = () => {
                this.props.history.push('/');
                this.emit('event:products:getById', item._id, 500);
            };
            return (
                <div className="news__product" onClick={click}>
                    <div className="news__product-image" style={style}/>
                    <div className="news__product-text-wrapper">
                        <div className="news__product-title">{item.title}</div>
                        <div className="news__product-price">{item.price}₽</div>
                    </div>
                </div>
            );
        });
    }

    private renderNews() {
        return this.state.news.map((item: INewsItem, key: number) => {
            const expand = () => {
                this.state.expandedNews.add(key);
                this.forceUpdate();
            };
            moment.locale('ru');
            return (
                <div key={key} className="news__item">
                    <span className="news__item-date">{moment.unix(item.date).format('ll')}</span>
                    {!!item.text &&
                    <div className={classnames('news__item-text', {'news__item-text_expanded': this.state.expandedNews.has(key)})}
                         onClick={expand}>{item.text}</div>}
                    {this.renderImages(item.images)}
                </div>
            );
        });
    }

    private renderImages(images: Array<string>) {
        if(!images || !images.length) {
            return null;
        }

        return (
            <div className="news__item-images-container">
                {images.map((url: string, key: number) => {
                    const viewPicture = () => {
                        const previewArray = images.map((url: string) => ({image: url}));
                        modalService.open((<PictureViewer selected={key}
                                                          images={previewArray}/>));
                    };
                    const style = {
                        backgroundImage: `url(${url})`
                    };
                    return (
                        <div key={key} style={style} onClick={viewPicture} className="news__item-image-wrapper">
                            <img className="news__item-image" src={url}/>
                        </div>
                    );
                })}
             </div>
         );
    }

    private getNews() {
        if(this.state.isFetching) {
            return;
        }
        this.state.isFetching = true;
        this.setState({isFetching: true});
        const {
            offset,
            limit
        } = this;
        return newsService.list({offset, limit})
            .then((news: Array<INewsItem>) => {
                const isFetchingAllowed = news.length === this.limit;
                this.offset += news.length;
                this.state.news.push(...news);
                this.setState({news: this.state.news, isFetchingAllowed})
            })
            .catch((e) => {
                console.log(e);
            })
            .then(() => {
                this.setState({isFetching: false});
            });
    }
}

export default News;
