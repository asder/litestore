import * as React from 'react';
import BaseComponent from '../../components/base-component';
import './contacts.scss';
import { YMaps, Map, Placemark, ZoomControl } from 'react-yandex-maps';
import { companyService, ICompanyInfo, IAddress, ICoord } from '../../services/company-service';
import * as classnames from 'classnames';

interface IState {
    addresses: Array<IAddress>;
    mapCenter?: ICoord
}

class Contacts extends BaseComponent {

    public state: IState = {
        addresses: [],
        mapCenter: [59.929363, 30.289682]
    };

    public componentDidMount() {
        companyService.getInfo()
            .then((companyInfo: ICompanyInfo) => {
                const newState: IState = {addresses: companyInfo.addresses};

                const addressWithCoords = companyInfo.addresses.find((address: IAddress) => {
                    return !!address.coords;
                });
                addressWithCoords && (newState.mapCenter = addressWithCoords.coords);

                this.setState(newState);
            });
    }

    public render() {
        return (
            <div className="contacts">
                <div className="contacts__map loader-spinner">
                    <YMaps>
                        <Map width={'100%'} height={'340px'}
                             state={{center: this.state.mapCenter, zoom: 16}}
                             defaultState={{center: [59.929363, 30.289682], zoom: 16}}>
                            <Placemark
                                geometry={{
                                    type: 'Point',
                                    coordinates: this.state.mapCenter
                                }}
                                properties={{
                                    hintContent: 'Собственный значок метки',
                                    balloonContent: 'Это красивая метка'
                                }}
                            />
                            <ZoomControl/>
                        </Map>
                    </YMaps>
                </div>
                <div className="contacts__address-block">
                    {this.renderAddresses()}
                </div>
            </div>
        );
    }

    private renderAddresses() {
        return this.state.addresses.map((address: IAddress, key: number) => {
            const addressClick = () => {
                if(address.coords) {
                    this.setState({mapCenter: address.coords})
                }
            };
            return (
                <div key={key}
                     className={classnames('contacts__address', {'contacts__address_has-coords': !!address.coords})}
                     onClick={addressClick}>
                    <div className="contacts__address-title">{address.title}</div>
                    <div className="contacts__address-address">{address.address}</div>
                    {address.time ?
                        <div className="contacts__address-time">
                            c {address.time.from} до {address.time.to}
                        </div> :
                        <div className="contacts__address-time">
                            Нет информации о времени работы
                        </div>
                    }
                </div>
            );
        });
    }
}

export default Contacts;
