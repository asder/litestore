import * as React from 'react';
import BaseComponent from '../../components/base-component';
import { companyService, ICompanyInfo } from '../../services/company-service';
import './delivery.scss';

interface IState {
    delivery?: string
}

class Delivery extends BaseComponent {

    public state: IState = {
    };

    public componentDidMount() {
        companyService.getInfo()
            .then((companyInfo: ICompanyInfo) => {
                if(companyInfo.delivery) {
                    this.setState({delivery: companyInfo.delivery})
                }
            });
    }

    public render() {
        return (
            <div className="delivery" dangerouslySetInnerHTML={{__html: this.state.delivery || ''}}>
            </div>
        );
    }
}

export default Delivery;
