import * as React from 'react';
import BaseComponent from '../../components/base-component';
import PictureViewer from '../../components/picture-viewer/picture-viewer';
import { modalService } from '../../services/modal-service';
import { galleryService, IGallery } from '../../services/gallery-service';
import './gallery.scss';
import Loader from "../../components/loader/loader";
import ScrollFetch from "../../components/scroll-fetch/scroll-fetch";

interface IState {
    galleryList: Array<IGallery>;
    isFetching: boolean;
    isFetchingAllowed: boolean;
}

class Gallery extends BaseComponent {

    public state: IState = {
        galleryList: [],
        isFetching: false,
        isFetchingAllowed: true
    };

    private offset: number = 0;
    private limit: number = 18;

    public componentDidMount() {
        this.getGalley();
    }

    public render() {
        const onFetch = () => this.getGalley();
        return (
            <div className="gallery">
                <div className="gallery__wrapper">
                    {this.renderPictures()}
                </div>
                {this.state.isFetching &&
                <Loader/>}
                {this.state.isFetchingAllowed &&
                <ScrollFetch onFetch={onFetch}/>}
            </div>
        );
    }

    private onPictureClick(key: number) {
        modalService.open((<PictureViewer selected={key}
                                          images={this.state.galleryList}/>));
    }

    private renderPictures() {
        return this.state.galleryList.map((gallery: IGallery, key: number) => {
            const picStyle = {
                backgroundImage: `url(${gallery.image})`
            };
            const openPic = () => this.onPictureClick(key);
            return (
                <div className="gallery__picture"
                     key={key}
                     onClick={openPic}
                     style={picStyle}/>
            );
        });
    }

    private getGalley() {
        if(this.state.isFetching) {
            return;
        }
        this.state.isFetching = true;
        this.setState({isFetching: true});
        const {
            offset,
            limit
        } = this;
        return galleryService.list({offset, limit})
            .then((gallery: Array<IGallery>) => {
                const isFetchingAllowed = gallery.length === this.limit;
                this.offset += gallery.length;
                this.state.galleryList.push(...gallery);
                this.setState({galleryList: this.state.galleryList, isFetchingAllowed})
            })
            .catch((e) => {
                console.log(e);
            })
            .then(() => {
                this.setState({isFetching: false});
            });
    }
}

export default Gallery;
