import * as React from 'react';
import BaseComponent from '../../components/base-component';
import Button from '../../components/button/button';
import Basket from '../../components/basket/basket';
import { modalService } from '../../services/modal-service';
import { productsService, IProduct } from '../../services/products-service';
import './shop.scss';
import PictureViewer from '../../components/picture-viewer/picture-viewer';
import ScrollFetch from '../../components/scroll-fetch/scroll-fetch';
import Loader from '../../components/loader/loader';
import * as classnames from 'classnames';
import { basketService } from '../../services/basket-service';

interface IState {
    products: Array<IProduct>;
    isFetchingAllowed: boolean;
    isFetching: boolean;
    expandedProduct: number;
}

class Shop extends BaseComponent {

    public state: IState = {
        products: [],
        isFetchingAllowed: true,
        isFetching: false,
        expandedProduct: -1
    };

    private offset: number = 0;
    private limit: number = 18;
    private category: string;
    private tags: string[] = [];

    public componentDidMount() {

        this.on('event:category:change', (category: string) => {
            if(this.isEventNow('event:products:getById')) {
                return;
            }
            this.category = category;
            this.offset = 0;
            this.setState({products: [], isFetchingAllowed: true});
            this.getProducts();
        });

        this.on('event:products:getById', (id: string) => {
            this.state.isFetching = true;
            this.setState({isFetching: true, isFetchingAllowed: false});
            productsService.getById(id)
                .then((product: IProduct) => {
                    this.setState({products: [product]});
                })
                .catch((e) => {
                    console.log(e);
                })
                .then(() => {
                    this.setState({isFetching: false});
                });
        });

        this.on('event:tags:change', (tags: string[]) => {
            if(this.isEventNow('event:products:getById')) {
                return;
            }
            this.tags = tags;
            this.offset = 0;
            this.setState({products: [], isFetchingAllowed: true});
            this.getProducts();
        });

        this.getProducts();
    }

    public render() {
        const onFetch = () => this.getProducts();
        return (
            <div className="shop">
                <div className="shop__products">
                    {this.renderProducts()}
                </div>
                {this.state.isFetching &&
                <Loader/>}
                {this.state.isFetchingAllowed &&
                <ScrollFetch onFetch={onFetch}/>}
            </div>
        );
    }

    private renderProducts() {
        return this.state.products.map((product: IProduct, key: number) => {
            const imgStyle = {
                backgroundImage: `url(${product.image})`
            };
            const openCart = () => {
                basketService.add(product);
                modalService.open((<Basket/>));
            };
            const viewPicture = () => {
                if(!product.images || !product.images.length) {
                    return;
                }
                const images = product.images.map((url) => {
                    return {image: url};
                });
                modalService.open((<PictureViewer selected={0}
                                                  images={images}/>));
            };
            const expand = () => {
                const expandedProduct = this.state.expandedProduct === key ? -1 : key;
                this.setState({expandedProduct});
            };
            const unExpand = () => {
                this.setState({expandedProduct: -1});
            };
            return (
                <div key={key} className="shop__product">
                    <div key={key} className={classnames('shop__product-wrapper', {'shop__product-wrapper_expanded': this.state.expandedProduct === key})}>
                        <div className="shop__product-title">{product.title}</div>
                        <div className="shop__product-img" style={imgStyle} onClick={viewPicture}/>
                        <div className="shop__product-description"
                             onMouseOut={unExpand}
                             onClick={expand}>
                            {product.description}
                        </div>
                        <div className="shop__product-price">
                            <span className="shop__product-price-text">{product.price}₽</span>
                            <Button type="blue" onClick={openCart} text="Купить"/>
                        </div>
                    </div>
                </div>
            );
        });
    }

    private async getProducts() {
        if(this.state.isFetching) {
            return;
        }
        this.state.isFetching = true;
        this.setState({isFetching: true});
        const {
            offset,
            limit,
            category,
            tags
        } = this;
        return productsService.list({offset, limit, category, tags})
            .then((products: Array<IProduct>) => {
                const isFetchingAllowed = products.length === this.limit;
                this.offset += products.length;
                this.state.products.push(...products);
                this.setState({products: this.state.products, isFetchingAllowed})
            })
            .catch((e) => {
                console.log(e);
            })
            .then(() => {
                this.setState({isFetching: false});
            });
    }
}

export default Shop;
