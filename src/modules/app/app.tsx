import * as React from 'react';
import { BrowserRouter as Router, Route, withRouter } from 'react-router-dom';
import './app.scss';
import Header from '../header/header';
import Shop from '../shop/shop';
import Gallery from '../gallery/gallery';
import News from '../news/news';
import Contacts from '../contacts/contacts';
import Delivery from '../delivery/delivery';
import { modalService, ModalServiceView } from '../../services/modal-service';
import { companyService, ICompanyInfo } from '../../services/company-service';
import { categoryService, ICategory } from '../../services/category-service';
import BaseComponent from "../../components/base-component";
import * as classnames from 'classnames';
import MobileApps from '../../components/mobile-apps/mobile-apps';
import {orgService} from '../../services/org-service';
import Form from '../../components/form/form';
import { productsService } from '../../services/products-service';
import {alertService} from "../../services/alert-service";

interface IState {
    categories: Array<ICategory>;
    selectedCategoryIndex: number;
    companyInfo: any;
    form: any;
}

class App extends BaseComponent {
    public state: IState = {
        categories: [],
        selectedCategoryIndex: -1,
        companyInfo: {},
        form: {
            errors: new Set(),
            fields: []
        }
    };

    public componentDidMount() {
        companyService.getInfo()
            .then((companyInfo: ICompanyInfo) => {
                if(companyInfo) {
                    this.setState({companyInfo});
                    // @ts-ignore
                    // document.querySelector('link[rel="shortcut icon"]').href = companyInfo.logo;
                }
            });
        orgService.getPublicOrg()
            .then((org: any) => {
                if(/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent)) {
                    if(/siteId=/i.test(location.search)) {
                        if (org.services.indexOf('mobileApp') === -1 || !org.isMobileApp) {
                            window.close();
                        }
                    } else {
                        if (org.services.indexOf('mobileApp') !== -1 && org.isMobileApp) {
                            modalService.open(<MobileApps/>);
                        }
                    }
                }
            });
        categoryService.list()
            .then((categories: Array<ICategory>) => {
                this.setState({categories})
            });
        productsService.getTags()
            .then((tags: string[]) => {
                this.state.form.fields = tags.map((tag: string) => {
                    return {
                        name: tag,
                        label: tag,
                        type: 'checkbox',
                        value: false,
                        onChange: (field: any, form: any) => {
                            const tags = form.fields.reduce((tags: string[], field: any) => {
                                if(field.value) {
                                    tags.push(field.name);
                                }
                                return tags;
                            }, []);
                            this.emit('event:tags:change', tags, 500);
                        }
                    };
                });
                this.update();
            });

        this.on('event:route:change', (location: any) => {
            if(this.isEventNow('event:products:getById')) {
                return;
            }
            let indexTo;
            if(location.pathname === '/') {
                if(this.state.selectedCategoryIndex === -2) {
                    indexTo = -1;
                }
            } else {
                indexTo = -2;
            }
            if(indexTo) {
                this.setState({selectedCategoryIndex: indexTo});
            }
            this.state.form.fields.forEach((field: any) => {
                field.value = false;
            });
            this.update();
        });

        this.on('event:products:getById', () => {
            this.setState({selectedCategoryIndex: -2});
        });

        if(location.search.match(/paymentStatus=fail/i)) {
            alertService.alert('К сожалению платеж не прошел, вскоре с вами свяжется наш менеджер.');
        } else if(location.search.match(/paymentStatus=success/i)) {
            alertService.alert('Спасибо, что оплатили заказ, вскоре с вами свяжется наш менеджер.');
        }
    }

    private update = () => {
        this.forceUpdate();
    };

    public render() {
        const onCategorySelect = (index: number) => {
            this.setState({selectedCategoryIndex: index});
        };
        return (
            <Router>
                <div>
                    <ScrollToTop/>
                    <Header/>
                    <ModalServiceView/>
                    <div className="app-container">
                        {this.state.companyInfo.promo && !this.state.companyInfo.hidePromo &&
                        <img className="app-container__promo" src={this.state.companyInfo.promo}/>}
                        <div className="app-container__wrapper">
                            <div className="app-container__filter-block">
                                <div className="app-container__filter">
                                    <Categories categories={this.state.categories}
                                                onSelect={onCategorySelect}
                                                selected={this.state.selectedCategoryIndex}/>
                                </div>
                                <Form form={this.state.form} update={this.update}/>
                            </div>
                            <div className="app-container__content">
                                <Route exact path='/' component={Shop}/>
                                <Route exact path='/gallery' component={Gallery}/>
                                <Route exact path='/news' component={News}/>
                                <Route exact path='/delivery' component={Delivery}/>
                                <Route exact path='/contacts' component={Contacts}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

const Categories = withRouter(class extends BaseComponent {
    public render() {
        let onClick = () => {
            if(this.props.location.pathname !== '/') {
                this.props.history.push('/');
            }
            this.props.onSelect(-1);
            this.emit('event:category:change', undefined, 500);
        };
        return (
            <div>
                <div onClick={onClick}
                     className={classnames('app-container__filter-category', {
                         'app-container__filter-category_selected': this.props.selected === -1
                     })}>Все товары</div>
                {this.props.categories.map((category: ICategory, key: number) => {
                    onClick = () => {
                        if(this.props.location.pathname !== '/') {
                            this.props.history.push('/');
                        }
                        this.props.onSelect(key);
                        this.emit('event:category:change', category.title, 500);
                    };
                    return (
                        <div key={key} onClick={onClick}
                             className={classnames('app-container__filter-category', {
                                 'app-container__filter-category_selected': this.props.selected === key
                             })}>{category.title}</div>
                    );
                })}
            </div>
        );
    };
});

const ScrollToTop = withRouter(class extends BaseComponent {
    public componentDidMount(): void {
        this.emit('event:route:change', this.props.location, 500);
    }

    public componentDidUpdate(prevProps: any) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            this.emit('event:route:change', this.props.location);
            window.scrollTo(0, 0);
        }
    }

    public render() {
        return null;
    }
});

export default App;
