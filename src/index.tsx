import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.scss';
import App from './modules/app/app';

declare const window: any;

const startApp = () => {
    ReactDOM.render(
        <App />,
        document.getElementById('root') as HTMLElement
    );
};

if(window.cordova) {
    document.addEventListener('deviceready', startApp, false);
} else {
    startApp();
}
