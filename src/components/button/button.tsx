import * as React from 'react';
import './button.scss';
import * as classnames from 'classnames';

const Button = ({type, text, onClick, disabled = false}: {type?: 'blue'| 'white', text: string, onClick: any, disabled?: boolean}) => {
    const classes = classnames('button', {
        button_blue: type === 'blue',
        button_white: type === 'white'
    });
    return (
        <button className={classes} disabled={disabled} onClick={onClick}>{text}</button>
    );
};

export default Button;