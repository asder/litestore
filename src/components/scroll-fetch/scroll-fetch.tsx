import BaseComponent from '../base-component';
import * as ReactDOM from 'react-dom';
import * as React from 'react';

class ScrollFetch extends BaseComponent {
    private element: any;
    private listener: any;

    constructor(props: any) {
        super(props);

        this.listener = () => {
            const rect = this.element.getBoundingClientRect();
            if (rect.top < window.innerHeight + 100) {
                this.props.onFetch();
            }
        };
    }

    public componentDidMount(): void {
        this.element = ReactDOM.findDOMNode(this);
        window.addEventListener('scroll', this.listener);
    }

    public componentWillUnmount() {
        super.componentWillUnmount();
        window.removeEventListener('scroll', this.listener);

    }

    public render() {
        return (<div/>);
    }

}

export default ScrollFetch;