import './mobile-apps.scss';
import BaseComponent from '../base-component';
import * as React from 'react';
import Button from '../button/button';
import {modalService} from '../../services/modal-service';
import {keyValueService} from '../../services/key-value-service';

declare const globals: any;

class MobileApps extends BaseComponent {

    public state: any = {
        isAndroid: /android/i.test(navigator.userAgent)
    };

    private key: string;

    constructor(...props: any) {
        super(...props);
        let key = '';
        let match;

        match = navigator.userAgent.match(/\(([^\)]+?)(?:\s*build[^\)]*)?\)/i);
        match && (key += match[1]);

        match = navigator.userAgent.match(/(AppleWebKit[^\s]+)/i);
        match && (key += match[1]);

        match = navigator.userAgent.match(/(Chrome[^\s]+)/i);
        match && (key += match[1]);

        match = navigator.userAgent.match(/(Mobile[^\s]*)/i);
        match && (key += match[1]);

        match = navigator.userAgent.match(/(Safari[^\s]+)/i);
        match && (key += match[1]);

        this.key = key + screen.width + screen.height;
    }

    private sendKey = (e: any) => {
        e.preventDefault();
        keyValueService.set(this.key, globals.siteId)
            .catch(e => console.log(e));
        window.open(e.currentTarget.href, '_blank');
    };

    public render() {
        return (
            <div className="mobile-apps">
                <div className="mobile-apps__header">Скачайте приложение</div>
                <div className="mobile-apps__body">
                    {this.state.isAndroid &&
                    <a href="https://play.google.com/store/apps/details?id=com.litestore.shop" target="_blank" onClick={this.sendKey}>
                        <img className="mobile-apps__body-img-andr" src="/static/images/google-play-badge.png"/>
                    </a>}
                    {!this.state.isAndroid &&
                    <a href="https://itunes.apple.com/us/app/litestore/id1458097811?l=ru&ls=1&mt=8" target="_blank" onClick={this.sendKey}>
                        <img className="mobile-apps__body-img-ios" src="/static/images/ios-badge.svg"/>
                    </a>}
                </div>
                <div className="order__footer">
                    <Button type="white" text="Закрыть" onClick={()=>{modalService.closeTop()}}/>
                </div>
            </div>
        );
    }
}


export default MobileApps;