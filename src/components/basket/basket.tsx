import './basket.scss';
import BaseComponent from '../base-component';
import * as React from 'react';
import Button from '../button/button';
import { modalService } from '../../services/modal-service';
import { basketService, IBasket } from '../../services/basket-service';
import Order from '../order/order';
import {alertService} from '../../services/alert-service';

declare const globals: any;

interface IState {
    basket: IBasket;
}

class Basket extends BaseComponent {

    public state: IState = {
        basket: basketService.getClone()
    };

    private order = () => {

        if(globals.type === 'temp') {
            alertService.confirm(
                'Это демо версия сайта, зарегистрируйтесь, чтобы увидеть все функции сайта.',
                'Зарегистрироваться',
                () => {
                    this.redirectToMain();
                });
        } else {
            modalService.closeTop();
            modalService.open((<Order/>));
        }
    };

    private redirectToMain = () => {
        // @ts-ignore
        const groupName = location.host.match(/^[^\.]+/)[0].replace(/_tmp$/, '');
        location.href = `https://litestore.me/main?authFailUrl=${encodeURIComponent(`/authorize?register&groupName=${groupName}`)}`;
    };

    public render() {
        return (
            <div className="basket">
                <div className="basket__header">Корзина покупок</div>
                <div className="basket__body">
                    {this.renderProducts()}
                    <div className="basket__products-total">
                        {Object.keys(this.state.basket.products).reduce((res: number, _id: string) => {
                            const product = this.state.basket.products[_id];
                            return res + product.item.price*product.count;
                        }, 0)} ₽
                    </div>
                </div>
                <div className="basket__footer">
                    <Button type="white" text="Продолжить покупки" onClick={()=>{modalService.closeTop()}}/>
                    {Object.keys(this.state.basket.products).length > 0 &&
                    <Button text="Перейти к оформлению" onClick={this.order}/>}
                </div>
            </div>
        );
    }

    private renderProducts() {
        if(!Object.keys(this.state.basket.products).length) {
            return (
                <div className="basket__products">
                    Корзина пуста.
                </div>
            );
        }

        return (
            <div className="basket__products-wrapper">
                <div className="basket__products">
                    <div className="basket__products-row basket__products-row_heading">
                        <div className="basket__products-cell basket__products-cell_remove"></div>
                        <div className="basket__products-cell basket__products-cell_name">Наименование</div>
                        <div className="basket__products-cell basket__products-cell_price">Цена</div>
                        <div className="basket__products-cell basket__products-cell_quantity">Кол-во</div>
                        <div className="basket__products-cell basket__products-cell_total">Сумма</div>
                    </div>
                    {Object.keys(this.state.basket.products).map((_id: string) => {
                        const product = this.state.basket.products[_id];
                        const remove = () => {
                            basketService.removeAllOf(product.item._id);
                            delete this.state.basket.products[_id];
                            this.forceUpdate();
                        };
                        const increase = () => {
                            basketService.add(product.item);
                            product.count++;
                            this.forceUpdate();
                        };
                        const reduce = () => {
                            basketService.remove(_id);
                            product.count = Math.max(product.count - 1, 1);
                            this.forceUpdate();
                        };
                        const imgStyle={
                            backgroundImage: `url(${product.item.image})`
                        };
                        return (
                            <div className="basket__products-row">
                                <div className="basket__products-cell basket__products-cell_remove"><span onClick={remove}>x</span></div>
                                <div className="basket__products-cell basket__products-cell_name">
                                    <div className="basket__products-image" style={imgStyle}/>
                                    {product.item.title}
                                </div>
                                <div className="basket__products-cell basket__products-cell_price">{product.item.price} ₽</div>
                                <div className="basket__products-cell basket__products-cell_quantity">
                                    <div className="basket__products-cell-quantity">
                                        <div className="basket__products-cell-quantity_top" onClick={increase}>+</div>
                                        <div className="basket__products-cell-quantity_middle">
                                            {product.count}
                                        </div>
                                        <div className="basket__products-cell-quantity_bottom" onClick={reduce}>-</div>
                                    </div>
                                </div>
                                <div className="basket__products-cell basket__products-cell_total">{product.item.price*product.count} ₽</div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default Basket;
