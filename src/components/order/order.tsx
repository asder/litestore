import './order.scss';
import BaseComponent from '../base-component';
import * as React from 'react';
import Button from '../button/button';
import { basketService } from '../../services/basket-service';
import {modalService} from '../../services/modal-service';
import {orgService} from '../../services/org-service';
import { orderService } from '../../services/order-service';
import Loader from '../loader/loader';
import { alertService } from '../../services/alert-service';
import {toQuery} from '../../services/api';
import Form from '../form/form';

declare const globals: any;

/*interface IState {
    phone: string;
    email: string;
    errorEmail?: boolean;
    errorPhone?: boolean;
    showEmailError?: boolean;
    showPhoneError?: boolean;
    loading?: boolean;
}*/

class ThxBox extends BaseComponent {
    public render() {
        return (
            <div className="order">
                <div className="order__header">Заказ оформлен</div>
                <div className="order__body">
                    <span className="order__thx">Спасибо за ваш заказ, в ближайшее время с вами свяжется наш менеджер.</span>
                </div>
                <div className="order__footer">
                    <Button text="Закрыть"
                            onClick={()=>modalService.closeTop()}/>
                </div>
            </div>
        );
    }
}

class OnlinePay extends BaseComponent {
    private payOnline = () => {
        location.href = `https://auth.robokassa.ru/Merchant/Index.aspx?${toQuery(this.props.options.robokassaParams)}`;
    };

    public render() {
        return (
            <div className="order">
                <div className="order__header">Оплата</div>
                <div className="order__body">
                    <span className="order__thx">Вы можете оплатить онайн любым удобным способом.</span>
                </div>
                <div className="order__footer">
                    <Button type="white" text="Оплатить потом" onClick={()=>{modalService.closeTop()}}/>
                    <Button text="Оплатить онлайн"
                            onClick={this.payOnline}/>
                </div>
            </div>
        );
    }
}

class Order extends BaseComponent {

    public state: any = {
        phone: '',
        email: '',
        org: {services: []},
        form: {
            errors: new Set(),
            fields: [
                {
                    name: 'phone',
                    type: 'phone'
                },
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'email'
                }
            ]
        }
    };

    public componentDidMount(): void {
        orgService.getPublicOrg()
            .then((org: any) => {
                this.setState({org});
            })
            .catch((e: any) => {
                alertService.alert('Не удалось получить информацию об организации.');
                console.log(e);
            });
    }

    public makeOrder = () => {
        this.setState({loading: true});

        const email = this.state.form.fields.find((field: any) => field.name === 'email').value;
        const phone = this.state.form.fields.find((field: any) => field.name === 'phone').value;

        const products = basketService.getClone().products;
        orderService.makeOrder({email, phone, products})
            .then(({robokassaParams}) => {
                basketService.clear();
                modalService.closeTop();
                if(this.state.org.services.indexOf('onlinePay') !== -1 && this.state.org.robokassa[globals.siteId]) {
                    const options = {robokassaParams: Object.assign({Email: email}, robokassaParams)};
                    modalService.open((<OnlinePay options={options}/>));
                } else {
                    modalService.open((<ThxBox/>));
                }

            })
            // @ts-ignore
            .catch((e: any) => {
                if(e.errorCode === 'NOT_PAYED') {
                    return alertService.alert('Пожалуйста продлите услугу пользования сайтом.');
                }
                alertService.alert('Не удалось разместить заказ, попробуйте еще раз.');
                console.log(e);
            })
            .then(() => {
                this.setState({loading: false})
            });
    };

    private update = () => {
        this.forceUpdate();
    };

    public render() {
        return (
            <div className="order">
                <div className="order__header">Оформление покупок</div>
                <div className="order__body">
                    <Form form={this.state.form} update={this.update}/>
                    {this.state.loading && <Loader/>}
                </div>
                <div className="order__footer">
                    <Button text="Оформить покупки"
                            disabled={this.state.form.errors.size || this.state.loading}
                            onClick={this.makeOrder}/>
                </div>
            </div>
        );
    }
}


export default Order;
