import './picture-viewer.scss';
import BaseComponent from '../base-component';
import * as React from 'react';

class PictureViewer extends BaseComponent {

    public state = {
        selected: 0
    };

    constructor(props: any) {
        super(props);
        if(props.selected >=0 && props.selected < props.images.length) {
            this.state.selected = props.selected;
        }
    }

    public componentWillReceiveProps(nextProps: Readonly<{ [p: string]: any }>, nextContext: any): void {
        this.setState({selected: nextProps.selected});
    }

    public render() {
        return (
            <div className="picture-viewer">
                <img className="picture-viewer__picture loader-spinner"
                     onClick={(e) => {this.switchPhoto(1);}}
                     src={this.props.images[this.state.selected].image}/>
            </div>
        );
    }

    private switchPhoto(direction: 1 | -1) {
        const selected = this.state.selected + direction;
        if(selected < 0 || selected >= this.props.images.length) {
            return;
        }
        this.setState({selected});
    }
}

export default PictureViewer;