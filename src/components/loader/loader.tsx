import * as React from 'react';
import './loader.scss';

const Loader = () => {
    return (
        <div className="loader loader-spinner"/>
    );
};

export default Loader;