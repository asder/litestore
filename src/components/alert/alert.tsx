import './alert.scss';
import BaseComponent from '../base-component';
import * as React from 'react';
import Button from '../button/button';
import { modalService } from '../../services/modal-service';

class Alert extends BaseComponent {

    private onCancel = () => {
        this.props.onCancel && this.props.onCancel();
        modalService.closeTop();
    };

    private onConfirm = () => {
        this.props.onConfirm && this.props.onConfirm();
        modalService.closeTop();
    };

    public render() {
        return (
            <div className="ui-alert">
                <div className="ui-alert__header"></div>
                <div className="ui-alert__body">
                    {this.props.text}
                </div>
                <div className="ui-alert__footer">
                    <Button text="Закрыть"
                            type="white"
                            onClick={this.onCancel}/>
                    {this.props.confirm &&
                    <Button text={this.props.confirm}
                            onClick={this.onConfirm}/>}
                </div>
            </div>
        );
    }
}


export default Alert;
