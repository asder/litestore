import './form.scss';
import * as React from 'react';
import BaseComponent from '../base-component';
import * as classnames from 'classnames';
import ReactPhoneInput from 'react-phone-input-2';
import * as EmailValidator from 'email-validator';
import Switch from 'react-toggle-switch';
import 'react-toggle-switch/dist/css/switch.min.css';
import Button from '../button/button';

export const buttonFormErrorValidator = (form: any, field: any) => 
    !form.errors.size || form.errors.size === 1 && form.errors.has(field);

class Form extends BaseComponent {
    public render() {
        return (
            <div className="ui-form">
                {this.props.form.fields.map((field: any, key: number) => {
                    let result: any = '';
                    switch (field.type) {
                        case 'email':
                            field.validator = () => EmailValidator.validate(field.value);
                        case 'text':
                            result = this.renderTextField(field, key);
                            break;
                        case 'phone':
                            field.validator = () => field.value && field.value.length >= 18;
                            result = this.renderPhoneField(field, key);
                            break;
                        case 'password':
                            result = this.renderPasswordField(field, key);
                            break;
                        case 'checkbox':
                            result = this.renderCheckbox(field, key);
                            break;
                        case 'switch':
                            result = this.renderSwitch(field, key);
                            break;
                        case 'textarea':
                            result = this.renderTextarea(field, key);
                            break;
                        case 'select':
                            result = this.renderSelect(field, key);
                            break;
                        case 'button':
                            result = this.renderButton(field, key);
                            break;
                        case 'component':
                            result = this.renderComponent(field, key);
                            break;

                    }
                    this.validateField(field);
                    return result;
                })}
            </div>
        );
    }

    private renderComponent(field: any, key: number) {
        return (
            <div key={key}>
                {field.component}
            </div>
        );
    }

    private renderButton(field: any, key: number) {
        return (
            <div key={key} className="ui-form__field-wrapper">
                <Button text={field.label}
                        disabled={field.error}
                        onClick={() => field.onClick(field, this.props.form)}/>
            </div>
        );
    }

    private renderSwitch(field: any, key: number) {
        const toggleValue = () => {
            field.value = !field.value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                <div className="ui-form__switch-wrapper">
                    <Switch onClick={toggleValue} on={field.value} className='ui-form__switch'/>
                    <div className="ui-form__switch-label">{field.label}</div>
                </div>
            </div>
        );
    }

    private renderCheckbox(field: any, key: number) {
        const toggleValue = () => {
            if(field.disabled) {
                return;
            }
            field.value = !field.value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                <div className={classnames('ui-form__checkbox-wrapper', {'ui-form__checkbox_disabled': field.disabled})}>
                    <div className={classnames('ui-form__checkbox', {'ui-form__checkbox_unchecked': !field.value})}
                        onClick={toggleValue}>
                        <svg xmlns="http://www.w3.org/2000/svg" focusable="false" viewBox="0 0 24 24">
                            <path className="icon__fill"
                                  d="M16.293 8.293l-6.294 6.294c.001-.001-2.292-2.294-2.292-2.294a1 1 0 0 0-1.414 1.414l2.294 2.294c.78.78 2.05.777 2.826 0l6.294-6.294a1 1 0 1 0-1.414-1.414z"/>
                        </svg>
                    </div>
                    <div className="ui-form__checkbox-label">{field.label}</div>
                </div>
            </div>
        );
    }

    private renderTextField(field: any, key: number) {
        const onBlur = () => {
            field.dirty = true;
            this.update();
        };
        const onChange = (e: any) => {
            field.value = e.target.value;
            this.validateField(field);
            this.update();
        };
        const onKeyPress = (e: any) => {
            field.onKeyPress && field.onKeyPress(e);
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                {field.label && <div className="ui-form__field-label">{field.label}</div>}
                <input type="text"
                   ref={field.ref}
                   className={classnames('ui-form__text', {'ui-form__text_error': field.dirty && field.error})}
                   onChange={onChange}
                   onBlur={onBlur}
                   onKeyPress={onKeyPress}
                   placeholder={field.placeholder}
                   disabled={field.disabled}
                   value={field.value}/>
            </div>
        );
    }

    private renderSelect(field: any, key: number) {
        const onBlur = () => {
            field.dirty = true;
            this.update();
        };
        const onChange = (e: any) => {
            field.value = e.target.value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                {field.label && <div className="ui-form__field-label">{field.label}</div>}
                <select
                    ref={field.ref}
                    className={classnames('ui-form__select', {'ui-form__select_error': field.dirty && field.error})}
                    onChange={onChange}
                    onBlur={onBlur}>
                    {field.placeholder && <option value='' disabled selected hidden>{field.placeholder}</option>}
                    {field.options.map((option: {value: any, title: string}, key: number) => {
                        return (
                            <option key={key} selected={option.value===field.value} value={option.value}>{option.title}</option>
                        );
                    })}
                </select>
            </div>
        );
    }

    private renderTextarea(field: any, key: number) {
        const onBlur = () => {
            field.dirty = true;
            this.update();
        };
        const onChange = (e: any) => {
            field.value = e.target.value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                {field.label && <div className="ui-form__field-label">{field.label}</div>}
                <textarea
                    ref={field.ref}
                    className={classnames('ui-form__textarea', {'ui-form__textarea_error': field.dirty && field.error})}
                    onChange={onChange}
                    onBlur={onBlur}
                    placeholder={field.placeholder}
                    value={field.value}/>
            </div>
        );
    }

    private renderPasswordField(field: any, key: number) {
        const onBlur = () => {
            field.dirty = true;
            this.update();
        };
        const onChange = (e: any) => {
            field.value = e.target.value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                {field.label && <div className="ui-form__field-label">{field.label}</div>}
                <input type="password"
                       ref={field.ref}
                       className={classnames('ui-form__text', {'ui-form__text_error': field.dirty && field.error})}
                       onChange={onChange}
                       onBlur={onBlur}
                       placeholder={field.placeholder}
                       value={field.value}/>
            </div>
        );
    }

    private renderPhoneField(field: any, key: number) {
        const onBlur = () => {
            field.dirty = true;
            this.update();
        };
        const onChange = (value: any) => {
            field.value = value;
            this.validateField(field);
            this.update();
        };
        return (
            <div key={key} className="ui-form__field-wrapper">
                {field.label && <div className="ui-form__field-label">{field.label}</div>}
                <ReactPhoneInput
                    defaultCountry='ru'
                    value={field.value}
                    containerClass={classnames('ui-form__phone', 'react-tel-input', {'ui-form__phone_error': field.dirty && field.error})}
                    onBlur={onBlur}
                    onChange={onChange}/>
            </div>
        );
    }

    private validateField(field: any) {
        if(field.validator) {
            field.error = !field.validator(this.props.form, field);
            this.props.form.errors = new Set(this.props.form.errors);
            const prevErrorsSize = this.props.form.errors.size;
            if(field.error) {
                this.props.form.errors.add(field)
            } else {
                this.props.form.errors.delete(field)
            }
            if(prevErrorsSize !== this.props.form.errors.size) {
                this.update();
            }
        }
        field.onChange && field.onChange(field, this.props.form);
    }

    private update = () => this.props.update();
}

export default Form;
