import { Api } from './api';

export interface IGallery {
    _id: string;
    image: string;
}

class GalleryService extends Api {
    public list({limit = 20, offset = 0}: {limit?: number, offset?: number}): Promise<Array<IGallery>> {
        return this.fetch(`/api/v1/shop/gallery?${this.toQuery({limit, offset})}`);
    }
}

export const galleryService = new GalleryService();
