import { Api } from './api';
import { IBasketProducts } from './basket-service';

class OrderService extends Api {

    public makeOrder({phone, email, products}: {phone: string, email: string, products: IBasketProducts}): Promise<any> {
        const body = {
            phone,
            email,
            products: Object.keys(products).map((key) => products[key])
        };
        return this.fetch('/api/v1/shop/make-order', {method: 'POST', body});
    }
}

export const orderService = new OrderService();