import { Api } from './api';

export interface IProduct {
    _id: string;
    title: string;
    image: string;
    description: string;
    price: number;
    images?: Array<string>;
    siteId?: string;
}

class ProductsService extends Api {
    public list({limit = 20, offset = 0, category, tags = []}: {limit?: number, offset?: number, category?: string, tags?: string[]}): Promise<IProduct[]> {
        return this.fetch(`/api/v1/shop/products?${this.toQuery({limit, offset, category, tags: tags.join(',')})}`);
    }

    public getById(id: string): Promise<IProduct> {
        return this.fetch(`/api/v1/shop/products/${id}`);
    }

    public getTags(): Promise<string[]> {
        return this.fetch(`/api/v1/shop/products-tags`);
    }
}

export const productsService = new ProductsService();
