import { Api } from './api';

class KeyValueService extends Api {
    public get(key: string): Promise<any> {
        return this.fetch(`/api/v1/get-value?${this.toQuery({key})}`);
    }

    public set(key: string, value: string): Promise<any> {
        const body = {
            key,
            value
        };
        return this.fetch(`/api/v1/set-value`, {method: 'POST', body});
    }
}

export const keyValueService = new KeyValueService();
