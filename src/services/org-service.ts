import { Api } from './api';

class OrgService extends Api {

    private getPublicOrgPromise: Promise<any>;

    public getPublicOrg(): Promise<any> {
        return this.getPublicOrgPromise || (this.getPublicOrgPromise = this.fetch(`/api/v1/shop/org-get`));
    }
}

export const orgService = new OrgService();
