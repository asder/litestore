import * as React from 'react';
import BaseComponent from '../components/base-component';

const changeEventName: string = 'event:modalService:change';

class ModalService extends BaseComponent {

    private components: any = [];

    public open(component: any) {
        const close = () => {
            this.close(component);
        };
        this.components.push(component);
        this.emit(changeEventName, this.components);

        return close;
    }

    public closeTop() {
        this.components.pop();
        this.emit(changeEventName, this.components);
    }

    public close(component: React.Component) {
        const index = this.components.indexOf(component);
        if(index >= 0) {
            this.components.splice(index, 1);
            this.emit(changeEventName, this.components);
        }
    }
}

export class ModalServiceView extends BaseComponent {
    public state: any = {
        components: []
    };
    constructor(props: any) {
        super(props);
        this.on(changeEventName, (components: any) => {
            this.setState({components});
        });
    }

    public render() {
        return this.state.components.map((component: any, key: number) => {
            const close = (e: any) => {
                if(e.target === e.currentTarget) {
                    modalService.close(component);
                }
            };
            return (
                <div key={key} onClick={close} className="modal-service">{component}</div>
            );
        });
    }
}

export const modalService = new ModalService({});