import Alert from '../components/alert/alert';
import {modalService} from './modal-service';
import * as React from 'react';

class AlertService {
    public alert(text: string) {
        return new Promise((resolve) => {
            modalService.open(<Alert text={text} onCancel={resolve}/>);
        });
    }
    public confirm(text: string, confirm: string, onConfirm?: ()=>void, onCancel?: ()=>void) {
        return new Promise((resolve, reject) => {
            const onConfirmPatched = () => {
                onConfirm && onConfirm();
                resolve();
            };
            const onCancelPathed = () => {
                onCancel && onCancel();
                reject()
            };
            modalService.open(<Alert text={text} confirm={confirm} onConfirm={onConfirmPatched} onCancel={onCancelPathed}/>);
        });
    }
}

export const alertService = new AlertService();
