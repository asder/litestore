import { Api } from './api';

export interface ICategory {
    _id: string;
    title: string;
}
class CategoryService extends Api {
    private listPromise: Promise<Array<ICategory>>;

    public list(): Promise<Array<ICategory>> {
        if(!this.listPromise) {
            this.listPromise = this.fetch('/api/v1/shop/categories');
        }
        return this.listPromise;
    }
}

export const categoryService = new CategoryService();