import { Api } from './api';

export interface INewsItem {
    _id: string;
    text: string;
    images: Array<string>;
    date: number;
}

class NewsService extends Api {
    public list({limit = 20, offset = 0}: {limit?: number, offset?: number}): Promise<Array<INewsItem>> {
        return this.fetch(`/api/v1/shop/news?${this.toQuery({limit, offset})}`);
    }
}

export const newsService = new NewsService();
