import { Api } from './api';

export type ICoord = [number, number];

export interface IAddress {
    title: string;
    address: string;
    time?: {from: number, to: number};
    coords?: ICoord
}

export interface ICompanyInfo {
    _id: string;
    logo: string;
    promo: string;
    addresses: Array<IAddress>,
    delivery: string;
    hidePromo: boolean;
    hideLogo: boolean;
    hideDelivery: boolean;
}

class CompanyService extends Api {
    private getInfoPromise: Promise<ICompanyInfo>;

    public getInfo(): Promise<ICompanyInfo> {
        if(!this.getInfoPromise) {
            this.getInfoPromise = this.fetch('/api/v1/shop/company-info');
        }
        return this.getInfoPromise;
    }
}

export const companyService = new CompanyService();
