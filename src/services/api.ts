declare const globals: any;

export const toQuery = (obj: {[key:string]: string | number | undefined}): string => {
    return Object.keys(obj)
        .filter((key: string) => {
            return obj[key] !== undefined;
        })
        .map((key: string) => {
            return `${encodeURIComponent(key)}=${encodeURIComponent(String(obj[key]))}`;
        })
        .join('&');
};

export class Api {
    private siteId: string;

    constructor() {
        this.siteId = globals.siteId;
    }

    public fetch(url: string, options: any = {}): Promise<any> {
        if(!options.headers) {
            options.headers = {};
        }
        if(!options.headers['Content-Type']) {
            options.headers['Content-Type'] = 'application/json; charset=utf-8';
        }
        options.headers['x-site-id'] = this.siteId;
        options.headers['x-session-id'] = localStorage.getItem('sessionId');
        options.headers = new Headers(options.headers);
        if(options.body && typeof options.body !== 'string') {
            options.body = JSON.stringify(options.body);
        }
        return fetch(url, options)
            .then((response) => {
                return response.json();
            })
            .catch((e) => {
                return Promise.reject('Invalid json');
            })
            .then((json) => {
                if(json.success) {
                    return json.result;
                }
                return Promise.reject(json);
            });
    }

    protected toQuery = toQuery;
}
