import { IProduct } from './products-service';

export interface IBasketProducts {
    [key: string]: {
        item: IProduct,
        count: number
    }
}

export interface IBasket {
    products: IBasketProducts;
}

class BasketService {

    private basket: IBasket;

    constructor() {
        this.basket = this.getClone();
    }

    getClone() {
        try {
            return JSON.parse(localStorage.getItem('basket') as string || '');
        } catch(e) {
            return {
                products: {}
            };
        }
    }

    public add(item: IProduct) {
        item = JSON.parse(JSON.stringify(item));
        delete item.siteId;
        if(!this.basket.products.hasOwnProperty(item._id)) {
            this.basket.products[item._id] = {
                item,
                count: 0
            };
        }
        this.basket.products[item._id].count++;
        localStorage.setItem('basket', JSON.stringify(this.basket));
    }

    public remove(id: string) {
        if(this.basket.products.hasOwnProperty(id)) {
            this.basket.products[id].count--;

            if(this.basket.products[id].count < 1) {
                delete this.basket.products[id];
            }
            localStorage.setItem('basket', JSON.stringify(this.basket));
        }
    }

    public removeAllOf(id: string) {
        delete this.basket.products[id];
        localStorage.setItem('basket', JSON.stringify(this.basket));
    }

    public clear() {
        this.basket.products = {};
        localStorage.setItem('basket', JSON.stringify(this.basket));
    }
}

export const basketService = new BasketService();